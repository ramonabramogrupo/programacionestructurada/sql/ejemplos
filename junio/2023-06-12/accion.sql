﻿USE compranp;

# CONSULTA DE DATOS ANEXADOS

/**
Crear la tabla para introducir los productos vendidos
**/

CREATE TABLE productosVendidos(
  id int AUTO_INCREMENT,
  nombre varchar(50),
  PRIMARY KEY(id)
);


-- INTRODUCIR EN LA TABLA ANTERIOR LOS PRODUCTOS QUE SE HAYAN VENDIDO
-- TENEMOS QUE UTILIZAR LAS TABLAS PRODUCTOS Y COMPRAN

-- OPCION JOIN
SELECT DISTINCT p.codPro,p.nombre 
FROM productos p 
  JOIN compran c ON p.codPro = c.codPro;

-- OPCION IN
SELECT p.codPro,p.nombre
FROM productos p
WHERE p.codPro IN (SELECT DISTINCT c.codPro FROM compran c);

-- EXISTS
SELECT p.codPro,p.nombre 
FROM productos p 
WHERE EXISTS (SELECT 1 FROM compran c WHERE c.codPro=p.codPro);

-- JOIN CON SUBCONSULTAS
SELECT C1.codPro,p.nombre
FROM productos p 
  JOIN (SELECT DISTINCT c.codPro FROM compran c) C1 USING (codPro);


-- CONSULTA DE DATOS ANEXADOS

INSERT INTO productosVendidos (id, nombre)
  SELECT C1.codPro,p.nombre
    FROM productos p 
    JOIN (SELECT DISTINCT c.codPro FROM compran c) C1 USING (codPro);

SELECT  * FROM productosVendidos v;

# CONSULTA

CREATE TABLE productosNoVendidos(
id int AUTO_INCREMENT,
nombre varchar(50),
PRIMARY KEY(id)
);
-- meto un producto que no se ha vendido
INSERT INTO productos (codPro, nombre, precio, numeroClientes)
  VALUES (101, 'raro', 10, 0);


-- INTRODUCIR EN LA TABLA ANTERIOR LOS PRODUCTOS QUE NO SE HAYAN VENDIDO
-- TENEMOS QUE UTILIZAR LAS TABLAS PRODUCTOS Y COMPRAN

-- OPCION LEFT JOIN
SELECT DISTINCT p.codPro,p.nombre 
FROM productos p 
  LEFT JOIN compran c ON p.codPro = c.codPro
WHERE c.codPro IS NULL;

-- OPCION NOT IN
SELECT p.codPro,p.nombre
FROM productos p
WHERE p.codPro NOT IN (SELECT DISTINCT c.codPro FROM compran c);

-- NOT EXISTS
SELECT p.codPro,p.nombre 
FROM productos p 
WHERE NOT EXISTS (SELECT 1 FROM compran c WHERE c.codPro=p.codPro);

-- LEFT JOIN CON SUBCONSULTAS
SELECT p.codPro,p.nombre
FROM productos p 
  LEFT JOIN (SELECT DISTINCT c.codPro FROM compran c) C1 USING (codPro)
WHERE c1.codPro IS NULL;

-- consulta de datos anexados
INSERT INTO productosNoVendidos (id, nombre)
  SELECT p.codPro,p.nombre
  FROM productos p 
    LEFT JOIN (SELECT DISTINCT c.codPro FROM compran c) C1 USING (codPro)
  WHERE c1.codPro IS NULL;

-- -------------------------------------
-- RESTAURAMOS LA BASE DE DATOS COMPRANP
-- -------------------------------------


# CONSULTA DE CREACION DE TABLA
-- CREAR UNA TABLA CON EL NOMBRE Y EL CODIGO DE LOS PRODUCTOS VENDIDOS

-- opcion 1
CREATE TABLE productosVendidos AS 
  SELECT p.codPro,p.nombre 
  FROM productos p 
  WHERE EXISTS (SELECT 1 FROM compran c WHERE c.codPro=p.codPro);  

-- opcion 2
-- coloco las claves y restricciones en la nueva tabla
DROP TABLE productosVendidos;
CREATE TABLE productosVendidos(
  codPro int,
  nombre varchar(50),
  PRIMARY KEY(codPro)
) AS 
  SELECT p.codPro,p.nombre 
  FROM productos p 
  WHERE EXISTS (SELECT 1 FROM compran c WHERE c.codPro=p.codPro);  

-- CONSULTA DE DATOS ANEXADOS
-- introducir los productos vendidos en la tabla productos
-- vendidos pero si algun producto vendido a cambiado de nombre
-- se actualiza en la tabla productos vendidos

INSERT INTO productosVendidos
  SELECT C1.codPro,p.nombre
    FROM productos p 
    JOIN (SELECT DISTINCT c.codPro FROM compran c) C1 USING (codPro)  
    ON DUPLICATE KEY UPDATE nombre=VALUES(nombre);



# CONSULTA DE CREACION DE TABLA
-- CREAR UNA TABLA CON EL NOMBRE Y EL CODIGO DE LOS PRODUCTOS no VENDIDOS

-- meto un producto que no se ha vendido
INSERT INTO productos (codPro, nombre, precio, numeroClientes)
  VALUES (101, 'raro', 10, 0);

-- la consulta de creacion
CREATE TABLE productosNoVendidos(
  codPro int,
  nombre varchar(50),
  PRIMARY KEY(codPro)
) AS 
  SELECT p.codPro,p.nombre
  FROM productos p
  WHERE p.codPro NOT IN (SELECT DISTINCT c.codPro FROM compran c);


-- consulta de datos anexados que me permita actualizar
-- INSERT+UPDATE
INSERT INTO productosNoVendidos 
  SELECT p.codPro,p.nombre
  FROM productos p 
    LEFT JOIN (SELECT DISTINCT c.codPro FROM compran c) C1 USING (codPro)
  WHERE c1.codPro IS NULL
  ON DUPLICATE KEY UPDATE nombre=VALUES(nombre);

SELECT * FROM productosNoVendidos nv;

SELECT * FROM productos p;

INSERT INTO productos (codPro, nombre, precio, numeroClientes)
  VALUES (102, 'rarito', 10, 0);


-- ejemplo de insert+update

INSERT INTO productos (codPro, nombre, precio, numeroClientes)
  VALUES (1, 'chorizo', 100, 0)
  ON DUPLICATE KEY UPDATE nombre=VALUES(nombre), precio=VALUES(precio), numeroClientes=0;

SELECT * FROM productos p;



## CONSULTA
-- CREAR UNA TABLA LLAMADA CLIENTES1
-- CON DOS CAMPOS CODCLI==>CODCLI Y NOMBRE ==>NOMBRE+APELLIDOS

CREATE TABLE clientes1(
  codcli int,
  nombre varchar(100),
  PRIMARY KEY(codcli)
) AS SELECT c.codCli,CONCAT_WS(" ",c.nombre,c.apellidos) AS nombre FROM clientes c;

SELECT * FROM clientes1 c;


