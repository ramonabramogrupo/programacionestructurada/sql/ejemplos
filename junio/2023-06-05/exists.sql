﻿USE piezas;

-- 1 - listar el codigo y el nombre de las piezas que han 
--    sido distribuidas alguna vez
SELECT DISTINCT p.P,p.nomp 
FROM piezas p 
  JOIN spj s ON p.P = s.p;

-- utilizando subconsultas
-- c1
-- piezas que han sido distribuidas
SELECT DISTINCT s.p FROM spj s;

-- c1 join piezas
SELECT p.P,p.nomp 
FROM piezas p
  JOIN (SELECT DISTINCT s.p FROM spj s) c1 USING(P);


-- piezas in c1
SELECT p.P,p.nomp 
FROM piezas p 
WHERE p.P IN (SELECT DISTINCT s.p FROM spj s);


-- consultas correlaccionadas
-- exists con spj

SELECT p.P,p.nomp 
FROM piezas p
WHERE EXISTS (SELECT 1 FROM spj s WHERE p.P=s.p);

-- 2 El codigo y el nombre de las piezas que no han sido distribuidas

-- left join
SELECT p.P, p.nomp 
FROM piezas p LEFT JOIN spj s ON p.P = s.p
WHERE s.p IS NULL;

-- utilizando subconsultas
-- c1
-- piezas que han sido distribuidas
SELECT DISTINCT s.p FROM spj s;

-- piezas left join c1
SELECT p.P,p.nomp 
FROM piezas p 
  LEFT JOIN (SELECT DISTINCT s.p FROM spj s) c1 USING(p)
WHERE c1.p IS NULL;

-- piezas not in c1
SELECT p.P,p.nomp 
FROM piezas p
WHERE p.P NOT IN (SELECT DISTINCT s.p FROM spj s);

-- consultas correlacionadas
-- NOT EXISTS
SELECT p.P,p.nomp 
FROM piezas p 
WHERE NOT EXISTS (SELECT 1 FROM spj s WHERE s.p=p.P);


