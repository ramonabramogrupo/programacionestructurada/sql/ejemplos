﻿USE ciclistas;
-- 1	Nombre y edad de los ciclistas que NO han ganado etapas

-- directamente

SELECT 
    c.nombre,c.edad 
  FROM ciclista c 
    LEFT JOIN etapa e ON c.dorsal = e.dorsal
  WHERE e.dorsal IS NULL;


-- optimizar 
-- c1 
-- ciclistas que han ganado etapas
SELECT DISTINCT e.dorsal FROM etapa e;

-- termino la consulta con un left join
SELECT 
    c.nombre,c.edad 
  FROM ciclista c 
    LEFT JOIN (SELECT DISTINCT e.dorsal FROM etapa e) c1 
    USING(dorsal)
  WHERE c1.dorsal IS NULL;

-- termino la consulta con un not in
SELECT 
    c.nombre,c.edad 
  FROM ciclista c
  WHERE dorsal NOT IN (SELECT DISTINCT e.dorsal FROM etapa e);





-- 2	Nombre y edad de los ciclistas que NO han ganado puertos

-- directamente

SELECT 
    c.nombre,c.edad 
  FROM ciclista c 
    LEFT JOIN puerto p ON c.dorsal = p.dorsal
  WHERE p.dorsal IS NULL;

-- Realizamos la consulta con una resta

-- c1 
-- ciclistas que han ganado puertos
SELECT DISTINCT p.dorsal FROM puerto p;

-- utilizamos la resta con left join
SELECT 
    c.nombre,c.edad 
  FROM ciclista c 
    LEFT JOIN (SELECT DISTINCT p.dorsal FROM puerto p) c1 
    USING(dorsal)
  WHERE c1.dorsal IS NULL;

-- utilizamos la resta con not in
SELECT c.nombre,c.edad 
FROM ciclista c 
WHERE dorsal NOT IN (SELECT DISTINCT p.dorsal FROM puerto p);

-- 3 Listar el director de los equipos que 
--   tengan ciclistas NO que hayan ganado NINGUNA etapa

-- directamente 

SELECT 
    DISTINCT e.director
  FROM equipo e 
  JOIN ciclista c ON e.nomequipo = c.nomequipo
  LEFT JOIN etapa et ON c.dorsal = et.dorsal
  WHERE et.dorsal IS NULL;

-- utilizando la resta

-- c1
-- ciclistas que han ganado etapas
SELECT e.dorsal FROM etapa e;

-- c2
-- equipos de los ciclistas que no han ganado etapas
SELECT 
    DISTINCT c.nomequipo 
  FROM ciclista c 
    LEFT JOIN (SELECT e.dorsal FROM etapa e) c1 
    USING(dorsal)
  WHERE c1.dorsal IS NULL;

-- terminarla
SELECT 
    e.director 
  FROM equipo e 
    JOIN (
          SELECT 
            DISTINCT c.nomequipo 
          FROM ciclista c 
            LEFT JOIN (SELECT e.dorsal FROM etapa e) c1 
            USING(dorsal)
          WHERE c1.dorsal IS NULL
    ) c2 USING(nomequipo);

SELECT * FROM equipo LEFT JOIN ciclista c ON equipo.nomequipo = c.nomequipo
WHERE c.nomequipo IS NULL;

-- 4- Dorsal y nombre de los ciclistas 
-- que NO hayan llevado algún maillot

-- directamente 
SELECT c.dorsal,c.nombre 
  FROM ciclista c 
    LEFT JOIN lleva l ON c.dorsal = l.dorsal
  WHERE l.dorsal IS NULL;

-- Utilizamos la resta

-- c1
-- dorsal de los ciclistas que han llevado alguna maillot
SELECT DISTINCT l.dorsal FROM lleva l;

-- implementamos la resta con left join
SELECT 
  c.dorsal,c.nombre 
FROM ciclista c 
  LEFT JOIN (SELECT DISTINCT l.dorsal FROM lleva l) c1
  USING(dorsal)
WHERE c1.dorsal IS NULL;

-- implementamos la resta con un not in
SELECT c.dorsal, c.nombre 
  FROM ciclista c 
  WHERE c.dorsal NOT IN (SELECT DISTINCT l.dorsal FROM lleva l);


-- 5   	Dorsal y nombre de los ciclistas que 
--      NO hayan llevado el maillot amarillo NUNCA

-- c1
-- codigo del maillot amarillo
SELECT m.código FROM maillot m WHERE m.color="amarillo";

-- c2
-- ciclistas que han llevado el maillot amarillo
-- implementando con un in
SELECT DISTINCT l.dorsal 
  FROM lleva l
  WHERE l.código IN (SELECT m.código FROM maillot m WHERE m.color="amarillo");

-- implementado con un join
SELECT 
    DISTINCT l.dorsal 
  FROM lleva l JOIN (SELECT m.código FROM maillot m WHERE m.color="amarillo") c1
    USING(código);

-- terminar 
-- restar todos los ciclistas menos los que han llevado el maillot amarillo

-- implementamos la resta con un left join
SELECT 
    c.dorsal,c.nombre 
  FROM ciclista c 
    LEFT JOIN (
        SELECT DISTINCT l.dorsal 
          FROM lleva l
          WHERE l.código IN (SELECT m.código FROM maillot m WHERE m.color="amarillo")
    ) c2 USING(dorsal)
  WHERE c2.dorsal IS NULL;

-- implementamos la resta con un not in
SELECT c.dorsal,c.nombre 
FROM ciclista c
WHERE c.dorsal NOT IN (
  SELECT DISTINCT l.dorsal 
    FROM lleva l
    WHERE l.código IN (SELECT m.código FROM maillot m WHERE m.color="amarillo")
);


-- 6  Indicar el numetapa de las etapas que NO tengan puertos

SELECT 
    e.numetapa 
  FROM etapa e 
    LEFT JOIN puerto p ON e.numetapa = p.numetapa
  WHERE p.numetapa IS NULL;

-- optimizar

-- c1
-- etapas que tienen puerto
SELECT DISTINCT p.numetapa FROM puerto p;

-- restamos etapas-c1

-- utilizando left join
SELECT e.numetapa 
FROM etapa e 
  LEFT JOIN (SELECT DISTINCT p.numetapa FROM puerto p) c1
  USING(numetapa)
WHERE c1.numetapa IS NULL;

-- utilizando not in
SELECT e.numetapa 
FROM etapa e
WHERE e.numetapa NOT IN (SELECT DISTINCT p.numetapa FROM puerto p);




-- 7 Indicar la distancia media de las etapas que NO tengan puertos

SELECT AVG(e.kms) distanciaMedia 
FROM etapa e 
  LEFT JOIN puerto p ON e.numetapa = p.numetapa
WHERE p.numetapa IS NULL;

-- optimizamos
-- c1
-- etapas que tienen puerto
SELECT DISTINCT p.numetapa FROM puerto p;

-- restar etapa - c1

-- left join
SELECT AVG(kms) media
FROM etapa e 
  LEFT JOIN (SELECT DISTINCT p.numetapa FROM puerto p) c1
  USING(numetapa)
WHERE c1.numetapa IS NULL;

-- not in
SELECT AVG(e.kms) media
FROM etapa e 
WHERE e.numetapa NOT IN (SELECT DISTINCT p.numetapa FROM puerto p);


-- 8 Listar el número de ciclistas que NO hayan ganado alguna etapa

-- directamente
SELECT COUNT(*) numero
FROM ciclista c LEFT JOIN etapa e ON c.dorsal = e.dorsal
WHERE e.dorsal IS NULL;

-- optimizamos

-- c1
-- ciclistas que han ganado etapas
SELECT COUNT(DISTINCT e.dorsal) numero FROM etapa e;

-- c2
-- ciclistas 
SELECT COUNT(*) numero FROM ciclista c;

-- c2-c1
-- resta escalar
SELECT 
  ((SELECT COUNT(*) numero FROM ciclista c)  
  - 
  (SELECT COUNT(DISTINCT e.dorsal) numero FROM etapa e)) numero;
  
-- 9 Listar el dorsal de los ciclistas que hayan ganado alguna etapa que no tenga puerto

SELECT DISTINCT e.dorsal 
FROM etapa e 
  LEFT JOIN puerto p ON e.numetapa = p.numetapa
WHERE p.numetapa IS NULL;


-- c1
-- etapas que tienen puerto
SELECT DISTINCT p.numetapa FROM puerto p;

-- resta
-- etapas - c1
-- sacar las etapas que no tienen puerto y dejar el ciclista

-- not in
SELECT DISTINCT e.dorsal 
FROM etapa e 
WHERE e.numetapa NOT IN (SELECT DISTINCT p.numetapa FROM puerto p);

-- LEFT join
SELECT DISTINCT e.dorsal 
FROM etapa e 
  LEFT JOIN (SELECT DISTINCT p.numetapa FROM puerto p) c1
  USING (numetapa)
WHERE c1.numetapa IS NULL;

-- 10 Listar el dorsal de los ciclistas 
--  que hayan ganado únicamente etapas que no tengan puertos

-- c1
-- listar el dorsal de los ciclistas que han ganado etapas con puertos
SELECT DISTINCT e.dorsal 
FROM etapa e JOIN puerto p ON e.numetapa = p.numetapa;

-- c2
-- listar ciclistas que han ganado etapas
SELECT DISTINCT e.dorsal FROM etapa e;

-- restar
SELECT * 
FROM (SELECT DISTINCT e.dorsal FROM etapa e) c2 
LEFT JOIN (SELECT DISTINCT e.dorsal 
FROM etapa e JOIN puerto p ON e.numetapa = p.numetapa) c1
USING(dorsal)
WHERE c1.dorsal IS NULL;

-- restar
SELECT DISTINCT c2.dorsal 
FROM (SELECT DISTINCT e.dorsal FROM etapa e) c2 
WHERE c2.dorsal NOT IN (
    SELECT DISTINCT e.dorsal 
    FROM etapa e JOIN puerto p ON e.numetapa = p.numetapa);


