﻿USE compranp;

SELECT * FROM productos p;

###############
-- CONSULTA
-- inicializar el numeroClientes de todos los productos a 0
UPDATE productos p 
  SET p.numeroClientes=0;

###############
-- CONSULTA
-- inicializar el numeroClientes a 1 de todos los productos que esten en la tabla compranp

-- metodo 1
-- no deberias utilizar un join a no ser que 
-- necesites un campo para el SET
UPDATE productos p JOIN compran c ON p.codPro = c.codPro
  SET p.numeroClientes=1;

-- metodo 2
-- es mejor que el anterior
UPDATE productos p 
  SET p.numeroClientes=1
  WHERE p.codPro IN (SELECT c.codPro FROM compran c);

-- metodo 3
-- este es otro metodo tambien bastante bueno

UPDATE productos p 
SET p.numeroClientes=1
WHERE EXISTS (SELECT 1 from compran c WHERE c.codPro=p.codPro);

###############
-- CONSULTA

-- AÑADIR UN CAMPO A LA TABLA PRODUCTOS DENOMINADO numeroUnidades
-- QUE ME INDIQUE EL NUMERO DE UNIDADES QUE SE HA COMPRADO DE ESE PRODUCTO
-- UTILIZANDO EL CAMPO CANTIDAD DE COMPRANP

-- añadir el campo
ALTER TABLE productos ADD COLUMN numeroUnidades int DEFAULT 0;

-- quiero actualizar el campo al valor solicitado

-- consulta de selecciona realizada directamente
SELECT c.codPro,SUM(c1.cantidad) suma 
FROM productos p 
  JOIN compran c ON p.codPro = c.codPro 
  JOIN compranp c1 ON c.idCompran = c1.idCompran
  GROUP BY c.codPro;

-- para calcular los valores que necesito me sobre productos
-- c1
SELECT c.codPro,SUM(c1.cantidad) suma 
FROM compran c 
JOIN compranp c1 ON c.idCompran = c1.idCompran
GROUP BY c.codPro;
-- que registros quiero actualizar??
-- todos los productos
-- esto es para saber si necesito where

-- realizo la de actualizacion
UPDATE productos p 
JOIN (SELECT c.codPro,SUM(c1.cantidad) suma 
FROM compran c 
JOIN compranp c1 ON c.idCompran = c1.idCompran
GROUP BY c.codPro) c1 USING(codPro)
  SET p.numeroUnidades=c1.suma;



###############
-- CONSULTA

-- actualizar el numero de productos de la tabla compran
-- utilizando cantidad de la tabla productos
-- lo que estoy calculando es obtener el numero de unidades vendidas 
-- al mismo cliente de ese producto

-- c1
-- obtengo el valor a actualizar
-- solo con la tabla compranp
SELECT c.idCompran, SUM(c.cantidad) suma 
  FROM compranp c
  GROUP BY c.idCompran;

-- que registros quiero actualizar??
-- todos los de compran

-- actualizacion
UPDATE compran c
JOIN (
  SELECT c.idCompran, SUM(c.cantidad) suma 
    FROM compranp c
    GROUP BY c.idCompran
  )c1 USING(idCompran)
SET c.numeroProductos=c1.suma;

###############
-- CONSULTA

-- CALCULAR EL PRECIO TOTAL DE LOS PRODUCTOS EN LA TABLA COMPRANP 
-- TENIENDO EN CUENTA LO SIGUIENTE:
-- PARA CALCULAR EL PRECIO TOTAL ES MULTIPLICAR EL PRECIO DEL 
-- PRODUCTO POR LAS UNIDADES

-- AL NO TENER QUE REALIZAR TOTALES PUEDO REALIZARLA SIN UTILIZAR UNA SUBCONSULTA

-- necesito calcular el valor
SELECT p.precio*c1.cantidad 
FROM productos p JOIN compran c ON p.codPro = c.codPro
JOIN compranp c1 ON c.idCompran = c1.idCompran;

-- quiero actualizar la tabla compranp y todos los registros

-- actualizacion
UPDATE productos p JOIN compran c ON p.codPro = c.codPro
JOIN compranp c1 ON c.idCompran = c1.idCompran
SET c1.total=p.precio*c1.cantidad;

###############
-- CONSULTA

-- CALCULAR EL PRECIO TOTAL DE LOS PRODUCTOS EN LA TABLA COMPRANP 
-- TENIENDO EN CUENTA LO SIGUIENTE:
-- SI EL NUMERO DE UNIDADES PEDIDO ES MENOR QUE 10 NO HAY DESCUENTO
-- SI EL NUMERO DE UNIDADES ESTA ENTRE 10 Y 30 EL DESCUENTO ES DEL 10%
-- SI EL NUMERO DE UNIDADES ES MAYOR QUE 30 EL DESCUENTO ES DEL 20%
-- PARA CALCULAR EL PRECIO TOTAL ES MULTIPLICAR EL PRECIO DEL PRODUCTO POR LAS UNIDADES

-- CONSULTA PARA CUANDO CANTIDAD<10  
    UPDATE 
        compranp c 
      JOIN 
        compran c1 ON c.idCompran = c1.idCompran
      JOIN 
        productos p1 ON c1.codPro = p1.codPro
      SET c.total=c.cantidad*p1.precio
      WHERE c.cantidad<10;

    -- CONSULTA PARA CUANDO LA CANTIDAD ESTA ENTRE 10 Y 30
    UPDATE 
        compranp c 
      JOIN 
        compran c1 ON c.idCompran = c1.idCompran
      JOIN 
        productos p1 ON c1.codPro = p1.codPro
      SET c.total=c.cantidad*p1.precio*0.9
      WHERE c.cantidad BETWEEN 10 AND 30;

    -- CONSULTA PARA CUANDO LA CANTIDAD ES MAYOR QUE 30
    UPDATE 
        compranp c 
      JOIN 
        compran c1 ON c.idCompran = c1.idCompran
      JOIN 
        productos p1 ON c1.codPro = p1.codPro
      SET c.total=c.cantidad*p1.precio*0.8
      WHERE c.cantidad>30;

-- podemos realizar todo en un solo update 
-- pero necesito la funcion condicional


-- realizo el calculo
SELECT p.precio * c1.cantidad * (1-IF(c1.cantidad<10,0,IF(c1.cantidad BETWEEN 10 AND 30,0.1,0.2))) calculo
FROM productos p 
JOIN compran c ON p.codPro = c.codPro 
JOIN compranp c1 ON c.idCompran = c1.idCompran;

-- coloco el update
UPDATE productos p 
JOIN compran c ON p.codPro = c.codPro 
JOIN compranp c1 ON c.idCompran = c1.idCompran
SET c1.total=p.precio*c1.cantidad*(1-IF(c1.cantidad<10,0,IF(c1.cantidad<=30,0.1,0.2)));


/**
CONSULTAS DE ELIMINACION
**/

###############
-- CONSULTA

-- ELIMINAR LOS PRODUCTOS CUYO PRECIO ES MAYOR O IGUAL QUE 20 EUROS

-- muestro los productos a borrar
SELECT * FROM productos p WHERE p.precio>=20;


-- CONSULTA DE ELIMINACION

-- sintaxis 1
DELETE FROM productos p WHERE p.precio>=20;

-- SINTAXIS 2
-- ESTA OPCION ES MEJOR QUE LA DEJEMOS PARA CUANDO TENGAMOS MAS DE 1 TABLA
-- EN LA CONSULTA DE ELIMINACION
DELETE p.* FROM productos p WHERE p.precio>=20;

-- SINTAXIS 3
-- ESTA OPCION ES MEJOR QUE LA DEJEMOS PARA CUANDO TENGAMOS MAS DE 1 TABLA
-- EN LA CONSULTA DE ELIMINACION
DELETE p FROM productos p WHERE p.precio>=20;

-- PODEMOS REALIZAR LAS CONSULTAS ANTERIORES SIN UTILIZAR ALIAS EN LA TABLA


###############
-- CONSULTA

-- Eliminar las compras de los productos cuyo precio es mayor o igual 20

-- SELECCION
SELECT * FROM compran c JOIN productos p ON c.codPro = p.codPro
WHERE p.precio>=20;

-- CONVERTIR LA DE SELECCION EN ELIMINACION
-- SINTAXIS 1
DELETE c FROM compran c JOIN productos p ON c.codPro = p.codPro
WHERE p.precio>=20;
-- SINTAXIS 2
DELETE c.* FROM compran c JOIN productos p ON c.codPro = p.codPro
WHERE p.precio>=20;

-- OPCION CON SUBCONSULTAS

-- WHERE + IN 
DELETE 
FROM compran 
WHERE codPro IN (SELECT p.codPro FROM productos p WHERE p.precio>=20);

-- WHERE ANTES DEL JOIN
DELETE c
FROM compran c 
JOIN (SELECT p.codPro FROM productos p WHERE p.precio>=20) C1 USING(codPro);

-- WHERE CON EXISTS
DELETE c
FROM compran c
WHERE EXISTS (SELECT 1 FROM productos p WHERE p.precio>=20 AND c.codPro=p.codPro);


###############
-- CONSULTA

-- ELIMINAR LAS COMPRAS DE LOS CLIENTES DE SANTANDER

DELETE c1
FROM clientes c 
JOIN poblacion p ON c.poblacion = p.idPob
JOIN compran c1 ON c.codCli = c1.codCli
WHERE p.nombre="santander";

-- realizarla con subconsultas

-- c1
SELECT p.idPob FROM poblacion p WHERE p.nombre="santander";

-- c2
SELECT c.codCli FROM clientes c WHERE c.poblacion=(SELECT p.idPob FROM poblacion p WHERE p.nombre="santander");

-- terminar con where + in 
DELETE 
FROM compran
WHERE codCli IN (SELECT c.codCli FROM clientes c WHERE c.poblacion=(SELECT p.idPob FROM poblacion p WHERE p.nombre="santander"));





