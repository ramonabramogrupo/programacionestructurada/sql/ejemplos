﻿USE ciclistas;

-- ciclistas que han ganado etapas y puertos

-- join
SELECT 
    DISTINCT e.dorsal 
  FROM etapa e JOIN puerto p USING(dorsal);


-- interseccion
-- c1
-- el dorsal de los ciclistas que han ganado etapas
SELECT DISTINCT e.dorsal FROM etapa e;

-- c2
-- el dorsal de los ciclistas que han ganado puertos
SELECT DISTINCT p.dorsal FROM puerto p;

-- solucion
SELECT DISTINCT e.dorsal FROM etapa e
INTERSECT 
SELECT DISTINCT p.dorsal FROM puerto p;

-- lo realizo con un natural join
SELECT 
    c1.* 
  FROM 
    (SELECT DISTINCT e.dorsal FROM etapa e) c1
    NATURAL JOIN (SELECT DISTINCT p.dorsal FROM puerto p) c2;


-- ciclistas que han etapas o puertos

-- c1
-- el dorsal de los ciclistas que han ganado etapas
SELECT DISTINCT e.dorsal FROM etapa e;

-- c2
-- el dorsal de los ciclistas que han ganado puertos
SELECT DISTINCT p.dorsal FROM puerto p;

-- union
SELECT DISTINCT e.dorsal FROM etapa e
UNION 
SELECT DISTINCT p.dorsal FROM puerto p;