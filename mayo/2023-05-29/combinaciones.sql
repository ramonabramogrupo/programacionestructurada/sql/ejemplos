﻿USE noticias;

-- no siempre es necesario combinar las tablas ni realizar
-- subconsultas

-- NUMERO DE AUTORES QUE HAN ESCRITO ALGUNA NOTICIA
SELECT COUNT(DISTINCT n.idAutor) numero FROM noticias n 

-- NUMERO DE NOTICIAS DE LAS CUALES CONOZCO EL AUTOR
SELECT 
    COUNT(*) numeroNoticias 
  FROM noticias n 
  WHERE n.idAutor IS NOT NULL;

SELECT 
    COUNT(n.idAutor) numeroNoticias
  FROM noticias n;

-- combinaciones internas

-- cuantos autores mayores de 50 años han escrito noticias 
SELECT COUNT(DISTINCT a.idAutor)  numeroAutores
  FROM noticias n 
    JOIN autores a ON n.idAutor = a.idAutor
  WHERE a.edad>50;

-- cuantas noticias han sido escritas por autores mayores de 50 años
SELECT COUNT(*)  numeroNoticias
  FROM noticias n 
    JOIN autores a ON n.idAutor = a.idAutor
  WHERE a.edad>50;

-- combinacion externa

-- indicame el numero de autores que no han escrito noticias
SELECT 
    COUNT(*) numeroAutores
  FROM autores a 
    LEFT JOIN noticias n ON a.idAutor = n.idAutor
  WHERE n.idAutor IS NULL;

-- combinacion interna
-- datos de los autores que han escrito noticias
SELECT 
    DISTINCT a.* 
  FROM autores a 
    JOIN noticias n ON a.idAutor = n.idAutor;

-- combinacion externa
-- datos de los autores que no han escrito noticias
-- izquierda
-- RESTA ==> autores - autores(noticias)
SELECT 
    a.* 
  FROM autores a 
    LEFT JOIN noticias n ON a.idAutor = n.idAutor
  WHERE n.idAutor IS NULL;

-- derecha
SELECT 
    a.* 
  FROM noticias n 
    RIGHT JOIN autores a ON n.idAutor = a.idAutor
  WHERE n.idAutor IS NULL;


-- combinacion total
-- quiero todas las noticias y todos los autores
-- al lado de cada noticia los datos del autor que la ha escrito

/*SELECT 
    * 
  FROM noticias n FULL JOIN autores a ON n.idAutor=a.idAutor;*/

-- emulamos el FULL JOIN con una UNION
-- LEFT JOIN UNION RIGTH JOIN

-- opcion 1
SELECT * FROM autores a LEFT JOIN noticias n ON a.idAutor = n.idAutor
UNION
SELECT * FROM autores a RIGHT JOIN noticias n ON a.idAutor = n.idAutor;

-- opcion 2
SELECT a.*,n.* FROM autores a LEFT JOIN noticias n ON a.idAutor = n.idAutor
UNION
SELECT a.*,n.* FROM noticias n LEFT JOIN autores a ON a.idAutor = n.idAutor;

