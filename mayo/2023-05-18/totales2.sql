﻿USE piezas;

/** 
Consulta de totales
**/

-- que colores tengo que tengan mas de 1 pieza que supere los 10 kg
-- group by por color
-- where >10kg
-- having >1pieza
-- totales (count)

SELECT 
    p.color,
    COUNT(*) nPiezas 
  FROM piezas p
  WHERE p.peso>10
  GROUP BY p.color
  HAVING nPiezas>1;

-- peso medio de las piezas por ciudad
SELECT 
    AVG(p.peso),
    p.ciudad
  FROM piezas p
  GROUP BY p.ciudad;

-- ciudades donde el peso medio de sus piezas supera los 15
SELECT 
    AVG(p.peso) pesoMedio,
    p.ciudad 
  FROM 
    piezas p
  GROUP BY
    p.ciudad
  HAVING 
    pesoMedio>15;









SELECT * FROM piezas p;