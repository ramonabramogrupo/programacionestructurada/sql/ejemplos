﻿USE piezas;

-- combinaciones
-- JOIN
-- conectar mas de una tabla en el FROM

-- combinacion interna
-- inner join
-- registros que estan en las dos tablas


-- todos los proyectos
SELECT * FROM proyectos p;
-- proyectos que se les ha suministrado alguna pieza
SELECT DISTINCT s.j FROM spj s;

-- ejercicio 1
-- todos los suministros(spj) con los datos de los proyectos suministrados

-- utilizando join + on
SELECT 
    * 
  FROM spj s JOIN proyectos p
  ON s.j = p.j;

-- utilizando join + using
-- esto solo se puede realizar cuando el campo de combinacion
-- se llama igual en las dos tablas
SELECT 
    * 
  FROM spj s JOIN proyectos p
  USING(j);

-- utilizar join + where
-- es preferible no utilizar este metodo
-- y dejar el where para seleccionar
SELECT 
    * 
  FROM spj s JOIN proyectos p
  WHERE s.j = p.j;

-- lo que realiza el inner join es esto
-- antiguamente los inner join se realizaban asi
-- , + where
SELECT 
    *
  FROM proyectos p, spj s 
  WHERE p.j=s.j;


-- ejercicio 2
-- nombre de las piezas que alguna vez han sido suministradas

-- utilizando on
SELECT 
    DISTINCT p.nomp 
  FROM piezas p JOIN spj s ON p.P = s.p ;

-- utilizando using
SELECT 
    DISTINCT p.nomp 
  FROM piezas p JOIN spj s USING(p);


-- ejercicio 3
-- nombre de las piezas rojas que se han suministrado alguna vez
SELECT 
    DISTINCT p.nomp 
  FROM piezas p JOIN spj s ON p.P = s.p
  WHERE p.color="ROJO";

-- EJERCICIO
-- nombre de las piezas que hayan sido suministradas a proyectos de Londres

-- utilizando on
SELECT 
    DISTINCT pi.nomp 
  FROM piezas pi 
    JOIN spj s ON pi.P = s.p
    JOIN proyectos pr ON s.j = pr.j
  WHERE 
    pr.ciudad="Londres";

-- utilizando using
SELECT 
    DISTINCT pi.nomp 
  FROM piezas pi 
    JOIN spj s USING(P)
    JOIN proyectos pr USING(j)
  WHERE 
    pr.ciudad="Londres";

-- esta opcion no es recomendable
SELECT 
    DISTINCT pi.nomp 
  FROM piezas pi 
    JOIN spj s 
    JOIN proyectos pr ON s.j = pr.j AND pi.P=s.p
  WHERE 
    pr.ciudad="Londres";

-- Ejercicio
-- los nombres de las piezas las cuales tienen la misma ciudad uqe un proyecto

SELECT 
   DISTINCT pi.nomp 
  FROM piezas pi JOIN proyectos p 
    ON p.ciudad=pi.ciudad;





