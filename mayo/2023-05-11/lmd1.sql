﻿-- teoria de manipulacion de datos
USE piezas;

-- consultas de seleccion simple
-- select campos from tabla alias

-- nombre y ciudad de los proveedores

SELECT 
    p.noms,p.ciudad -- campos
  FROM 
    proveedores p; -- tabla

-- ciudad de los proveedores
-- la clasusula distinct quita repetidos

SELECT 
    DISTINCT p.ciudad 
  FROM 
    proveedores p;

-- color y peso de las piezas
-- colocamos un alias a los campos
-- nombreCampo [AS] alias

SELECT 
    p.color colorPieza,
    p.peso pesoPieza
  FROM 
    piezas p;

-- ordenar los resultados
-- ORDER BY campo ASC|DESC,campo ASC|DESC, ...
-- select campo1,campo2, .... from tabla order by campo ASC|DESC

-- proyectos ordenados por piezasTotales

SELECT 
    * 
  FROM 
    proyectos p
  ORDER BY 
    p.piezasTotales;

-- nombre de las piezas ordenadas por nombre
SELECT 
    p.nomp 
  FROM piezas p 
  ORDER BY p.nomp;

-- vamos a realizar una serie de filtrados
-- where
-- where pregunta
-- select campo from tabla where pregunta order by campo ASC|DESC

-- nombre de proveedores que viven en londres
SELECT 
    p.noms
  FROM proveedores p
  WHERE 
    p.ciudad="Londres";

-- operadores logicos
-- NOT (complementar)
-- AND (Y, solo devuelve verdadero en caso de que todos las preguntas sean verdaderas)
-- OR (O, devuelve verdadero en cuanto una pregunta sea verdadera)
-- WHERE pregunta1 AND|OR pregunta2
-- WHERE NOT pregunta1

-- listas los proveedores de Londres y Paris

SELECT 
    * 
  FROM proveedores p
  WHERE
    p.ciudad="Londres" OR p.ciudad="Paris";

-- todas las piezas de color gris o rojo
SELECT 
    * 
  FROM piezas p
  WHERE p.color="gris" OR p.color="rojo";


-- piezas que sean grises o rojas y de londres
-- el operador AND tiene mas prioridad que el 
-- operador OR
-- para cambiar la prioridad utilizo los parentesis

SELECT 
    * 
  FROM piezas p
  WHERE 
    (p.color="rojo" OR p.color="gris")
    AND
    p.ciudad="Londres";

-- operador logicos avanzados
-- where campo in(valor1,valor2,valor3,...)
-- where campo between valorMin and valorMax

-- WHERE p.color="rojo" OR p.color="gris"
-- WHERE p.color IN("rojo","gris")

-- WHERE edad>=18 AND edad<=30
-- WHERE edad BETWEEN 18 AND 30

-- realizamos la consulta anterior con el IN

SELECT 
    * 
  FROM piezas p
  WHERE 
    p.color IN("Rojo","Gris")
    AND
    p.ciudad="Londres";

-- proveedores cuyo estado esta entre 20 y 50 (incluidos)
-- AND
-- BETWEEN

-- CON AND
SELECT 
    * 
  FROM proveedores p
  WHERE 
    p.estado>=20 AND p.estado<=50;

-- CON BETWEEN
SELECT 
    * 
  FROM proveedores p
  WHERE 
    p.estado BETWEEN 20 AND 50;

-- comodines
-- sustituir en una comparacion 0 o n caracteres
-- para comparar
-- operadores comparacion basicos (=,>,<,>=,<=,<>)
-- operador comparacion avanzado (LIKE)
-- where campo LIKE "expresion con comodines"
-- comodines 
-- _ : sustituye 1 caracter
-- % : sustituye 0 o n caracteres

-- piezas cuyo nombre comience por A
SELECT 
    * 
  FROM piezas p
  WHERE 
    p.nomp LIKE "a%";

-- piezas cuyo nombre comience por a o por m

SELECT 
    * 
  FROM piezas p
  WHERE
    p.nomp LIKE 'a%' OR p.nomp LIKE 'm%';
    
-- nombre de las piezas cuyo nombre esta entre a y m (incluidas)

SELECT 
    * 
  FROM piezas p
  WHERE 
      p.nomp >="a" AND p.nomp<"n";

SELECT 
    * 
  FROM piezas p
  WHERE 
      p.nomp BETWEEN "a" AND "n";
      
-- las funciones son herramientas para realizar calculos
-- en las expresiones  

-- select campo,expresion from tabla where expresion order BY campo
-- en las expresiones puedo realizar calculos con operadores y funciones

-- consulta de campo calculado
-- select expresion from tabla

-- listar el primer caracter de los nombres de las piezas y el nombre
SELECT 
    LEFT(p.nomp,1) primerCaracter,
    p.nomp  
  FROM piezas p;

-- ahora vamos a utilizar la funcion en el where
-- listar las piezas cuyo nombre comience por A
-- sin utilizar like
SELECT 
    * 
  FROM piezas p
  WHERE 
    LEFT(p.nomp,1)="a";

-- listar las piezas cuyo nombre comience por a o por m
-- sin utilizar like

SELECT 
    * 
  FROM 
    piezas p
  WHERE 
    LEFT(p.nomp,1) IN('a','m');




















