﻿USE libros;

SELECT * FROM autores a;
SELECT * FROM libros l;

/**
Cuando en el from necesito unir tablas utilizo combinacion
JOIN

INNER JOIN (COMBINACION INTERNA) : JOIN
OUTER JOIN (COMBINACION EXTERNA) : LEFT JOIN, RIGHT JOIN, FULL JOIN
NATURAL JOIN 
CROSS JOIN (PRODUCTO CARTESIANO)
STRAIGHT JOIN
**/

/**
COMBINACION INTERNA
**/

-- INNER + ON
SELECT 
    * 
  FROM autores a JOIN libros l 
    ON a.idAutor = l.idAutor;

-- INNER + USING
SELECT 
    * 
  FROM autores a JOIN libros l
    USING(idAutor);

-- , + WHERE
-- producto cartesiano y no vamos a realizar asi el join
SELECT 
    * 
  FROM autores a, libros l
  WHERE a.idAutor=l.idAutor;

-- inner + where
-- este metodo tampoco lo vamos a utilizar 
-- dejamos el where para seleccionar

SELECT 
    * 
  FROM autores a JOIN libros l 
    WHERE a.idAutor = l.idAutor;

-- ejercicio
-- libros de mas de 500 hojas con el nombre de su autor
SELECT 
    l.*,
    a.nombre nombreAutor
  FROM libros l JOIN autores a ON l.idAutor = a.idAutor
  WHERE l.paginas>500;

-- ejercicio
-- numero de autores
SELECT 
    COUNT(*) numeroAutores
  FROM autores a;

-- ejercicio 
-- numero de autores que hayan escrito libros
SELECT 
    COUNT(DISTINCT l.idAutor)
  FROM libros l;

-- EJERCICIO
-- NUMERO DE AUTORES QUE NUNCA HAN ESCRITO UN LIBRO

SELECT 
 (SELECT COUNT(*) numeroAutores FROM autores a)-
 (SELECT COUNT(DISTINCT l.idAutor) FROM libros l);
    
-- EJERCICIO
-- NOMBRE Y PAGINAS DE LOS LIBROS QUE HAYAN SIDO
-- ESCRITOS POR AUTORES DE SANTANDER O REINOSA

SELECT 
    l.nombre,l.paginas 
  FROM libros l JOIN autores a ON l.idAutor = a.idAutor
  WHERE a.poblacion IN ('SANTANDER','REINOSA');

-- EJERCICIO
-- EL NOMBRE DEL AUTOR QUE HA ESCRITO EL LIBRO MAS LARGO

-- ESTA SOLUCION ES VIABLE PERO PUEDE 
-- FALLAR SI TENEMOS MAS DE UN AUTOR QUE HA 
-- ESCRITO LIBROS CON EL MAXIMO NUMERO DE PAGINAS

SELECT 
    a.nombre,l.paginas 
  FROM libros l JOIN autores a ON l.idAutor = a.idAutor
  ORDER BY l.paginas DESC LIMIT 1;

-- NUMERO DE MAXIMO DE PAGINAS 
SELECT MAX(l.paginas) maximo FROM libros l;

-- sacar los autores que han escrito estos libros
SELECT 
    a.nombre,l.paginas 
  FROM libros l 
    JOIN (SELECT MAX(l.paginas) maximo FROM libros l) C1
    ON C1.maximo=l.paginas
    JOIN autores a ON l.idAutor = a.idAutor;








