﻿USE piezas;

/** 
Consulta de totales
**/

-- utilizar las funciones de totales
-- max
-- min
-- avg
-- count
-- sum

-- podemos necesitar las clausulas
-- group by (agrupar)
-- having  (filtrar)

-- select campo, campo1 from tabla where expresion order by campo
-- select funcionTotales() from tabla where expresion group by campo having expresion

-- numero de piezas

SELECT 
    COUNT(*) npiezas -- funcion de totales
  FROM piezas p;

-- peso mayor de las piezas
SELECT 
    MAX(p.peso) pesoMaximo
  FROM piezas p;

-- nombre de la pieza cuyo peso es maximo

-- no es correcto
SELECT 
    MAX(p.peso) pesoMaximo,
    p.nomp
  FROM piezas p;

-- podemos utilizar order + limit
SELECT * FROM piezas p ORDER BY peso DESC LIMIT 1;

-- solucion con subconsultas

-- c1
-- peso maximo
SELECT MAX(p.peso) pesoMaximo FROM piezas p;

-- consulta final
SELECT 
    * 
  FROM piezas p 
  WHERE peso=(SELECT MAX(p.peso) pesoMaximo FROM piezas p);


-- indicar el numero de piezas cuyo peso es mayor que 15
SELECT 
    COUNT(*) npiezas 
  FROM piezas p 
  WHERE 
    p.peso>15;

-- cuantos proveedores no son de paris

-- NOT =
SELECT 
    COUNT(*) total 
  FROM proveedores p
  WHERE 
    NOT p.ciudad ="paris" ;

-- <>
SELECT 
    COUNT(*) total
  FROM proveedores p
  WHERE 
    p.ciudad<>"paris" ;

-- cuantos proveedores no son ni de paris ni de londres

-- utilizando NOT
SELECT 
    COUNT(*) total 
  FROM proveedores p
  WHERE
    NOT(p.ciudad="paris" OR p.ciudad="Londres");

-- sin utilizar NOT
SELECT 
    COUNT(*) total
  FROM proveedores p
  WHERE
    p.ciudad<>"paris" AND p.ciudad<>"Londres";

-- INDICA CUANTOS PROVEEDORES HAY POR CIUDAD

SELECT 
    COUNT(*) numero,    
    p.ciudad 
  FROM proveedores p
  GROUP BY p.ciudad;


-- indicar el peso maximo de las piezas por color
SELECT 
    p.color,
    MAX(p.peso) pesoMaximo 
  FROM piezas p
  GROUP BY p.color;

-- HAVING (PARA FILTRAR POR EL TOTAL CALCULADO)

-- indica las poblaciones donde hay mas de 1 proveedor
SELECT 
    p.ciudad,
    COUNT(*) numero
  FROM proveedores p
  GROUP BY
    p.ciudad
  HAVING 
    numero>1;

-- que colores tengo que tengan mas de 1 pieza que supere los 10 kg
-- group by por color
-- where >10kg
-- having >1pieza
-- totales (count)







SELECT * FROM piezas p;