﻿DROP DATABASE IF EXISTS ejemploClase3;
CREATE DATABASE ejemploClase3;
USE ejemploClase3;

# tabla alumno
CREATE TABLE alumno(
  expediente int AUTO_INCREMENT,
  nombre varchar(100),
  apellidos varchar(200),
  fechaNac date,
  PRIMARY KEY(expediente)
);

# fin tabla alumno

# tabla esDelegado
CREATE TABLE esDelegado(
  expedienteDelegado int,
  expedienteAlumno int,
  PRIMARY KEY(expedienteDelegado,expedienteAlumno),
  CONSTRAINT uk1 UNIQUE KEY(expedienteAlumno)
);
# fin tabla esDelegado

# tabla cursa
CREATE TABLE cursa(
  expedienteAlumno int,
  codigoModulo int,
  PRIMARY KEY(expedienteAlumno,codigoModulo)
);

# fin tabla cursa

# tabla modulo
CREATE TABLE modulo(
  codigo int AUTO_INCREMENT,
  nombre varchar(100),
  dniProfesor varchar(10) NOT NULL,
  PRIMARY KEY(codigo)
);
  
# fin tabla modulo

# tabla profesor
CREATE TABLE profesor(
  dni varchar(20),
  nombre varchar(100),
  direccion varchar(200),
  tfno varchar(15),
  PRIMARY KEY (dni)
);
# fin tabla profesor

/*
Comienzo a realizar las claves ajenas
*/

# tabla esDelegado
ALTER TABLE esDelegado 
  ADD CONSTRAINT fkesDelegadoAlumno FOREIGN KEY(expedienteDelegado)
    REFERENCES alumno(expediente) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT fkesDelegadoAlumno2 FOREIGN KEY(expedienteAlumno)
    REFERENCES alumno(expediente) ON DELETE RESTRICT ON UPDATE RESTRICT;

# tabla cursa
ALTER TABLE cursa
  ADD CONSTRAINT fkCursaAlumno FOREIGN KEY (expedienteAlumno)
    REFERENCES alumno(expediente) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT fkCursaModulo FOREIGN KEY (codigoModulo)
    REFERENCES modulo(codigo) ON DELETE RESTRICT ON UPDATE RESTRICT;

# tabla modulo
ALTER TABLE modulo 
  ADD CONSTRAINT fkModuloProfesor FOREIGN KEY (dniProfesor)
    REFERENCES profesor(dni) ON DELETE RESTRICT ON UPDATE RESTRICT;

/*
fin de las claves ajenas
*/

/**
Comenzamos a introducir datos
**/

# tabla alumnos
INSERT INTO alumno ( nombre, apellidos, fechaNac) VALUES 
  ('eva', 'gomez','1990-1-1'),
  ('susana','lopez','1998-8-10'),
  ('jorge','vazquez','200-7-9');

# tabla profesores
INSERT INTO profesor (dni, nombre, direccion, tfno) VALUES 
  ('10101010a', 'ramon', 'vargas 1', '123456789'),
  ('20202020b','Daniel','isaac 23','789456123');

# tabla modulos
INSERT INTO modulo (codigo, nombre, dniProfesor) VALUES 
  (1, 'Excel', '10101010a'),
  (2, 'Word','10101010a'),
  (3,'Access','20202020b');

# tabla cursa
INSERT INTO cursa (expedienteAlumno, codigoModulo) VALUES 
  (1,1),
  (1,2),
  (1,3),
  (2,1),
  (2,2),
  (3,1);

#tabla esDelegado
INSERT INTO esDelegado (expedienteDelegado, expedienteAlumno) VALUES 
  (1, 2),
  (1, 3);






/**
Fin de introducir datos
**/