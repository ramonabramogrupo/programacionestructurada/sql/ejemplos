﻿DROP DATABASE IF EXISTS ejemploClase2;
CREATE DATABASE ejemploClase2;
USE ejemploClase2;

/*
  tabla clientes
*/
CREATE TABLE clientes(
  dni varchar(10),
  nombre varchar(100),
  apellidos varchar(200),
  fechaNac date,
  tfno varchar(15),
  PRIMARY KEY (dni)
);

/*
fin de la tabla clientes
*/

/*
tabla compras
*/
CREATE TABLE compras(
  dniCliente varchar(10),
  codigoProducto int,
  PRIMARY KEY(dniCliente,codigoProducto)
);

/*
fin de la tabla compras
*/

/**
tabla productos
**/

CREATE TABLE productos(
  codigo int,
  nombre varchar(200),
  precio float,
  PRIMARY KEY(codigo)
);

/*
fin de la tabla productos
*/

/**
tabla suministra
**/

CREATE TABLE suministra(
  codigoProducto int,
  nifProveedor varchar(10),
  PRIMARY KEY(codigoProducto,nifProveedor),
  CONSTRAINT uk1 UNIQUE KEY (codigoProducto)
);

/**
fin de la tabla suministra
**/

/** 
tabla proveedores
**/

CREATE TABLE proveedores(
  nif varchar(20),
  nombre varchar(100),
  direccion varchar(200),
  PRIMARY KEY(nif)
);

/**
fin de la tabla proveedores

**/

/**
Realizo las claves ajenas
**/

/**
claves ajenas de compra
**/
ALTER TABLE compras
  ADD CONSTRAINT fkCompraCliente FOREIGN KEY (dniCliente)
    REFERENCES clientes(dni) ON DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT fkCompraProductos FOREIGN KEY (codigoProducto)
    REFERENCES productos(codigo) ON DELETE RESTRICT ON UPDATE RESTRICT;

/**
fin de las claves ajenas de compra
**/

/**
claves ajenas de suministra
**/

ALTER TABLE suministra 
  ADD CONSTRAINT fkSuministraProductos FOREIGN KEY(codigoProducto)
    REFERENCES productos(codigo) on DELETE RESTRICT ON UPDATE RESTRICT,
  ADD CONSTRAINT fkSuministraProveedor FOREIGN KEY(nifProveedor)
    REFERENCES proveedores(nif) ON DELETE RESTRICT ON UPDATE RESTRICT;

/**
fin de las claves ajenas de suministra
**/

/**
INTRODUCIR REGISTROS
**/

# productos

INSERT INTO productos (codigo, nombre, precio) VALUES
  (1, 'lechuga', 1),
  (2,'tomate',2),
  (3,'chorizo',4.6);

# fin de productos

# proveedores
INSERT INTO proveedores (nif, nombre, direccion) VALUES 
  ('20202020a', 'luisa', 'vargas 1'),
  ('21212121b','jose','lopez 2');

# fin de proveedores

# clientes
INSERT INTO clientes (dni, nombre, apellidos, fechaNac, tfno) VALUES 
  ('10101010a', 'jorge', 'lopez', '1990/2/4', '456456456'),
  ('12121212b','ana','rodriguez','2000/4/5','654789789');

# fin de clientes

# suministra
INSERT INTO suministra (codigoProducto, nifProveedor) VALUES 
  (1, '20202020a'),
  (2,'20202020a'),
  (3,'21212121b');

# fin suministra

# compra
INSERT INTO compras (dniCliente, codigoProducto) VALUES 
  ('10101010a', 1),
  ('10101010a', 2),
  ('10101010a', 3),
  ('12121212b',1);

# fin compra