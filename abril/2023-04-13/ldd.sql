﻿/*
  Ejercicio ejemplo 1
*/
DROP DATABASE IF EXISTS ejemploClase; -- eliminar base de datos
CREATE DATABASE ejemploClase; # crear base de datos

USE ejemploClase; -- seleccionar la base de datos

/*
creando la tabla de coches
*/

CREATE TABLE coches(
  id int ,                -- campo tipo entero
  marca varchar(20),      -- campo de tipo texto
  modelo varchar(200),    -- campo texto
  añoCompra year,         -- campo año
  usado bool,             -- booleano (true/false)
  propietario int,        -- campo de tipo entero que va a ser la clave ajena
  PRIMARY KEY(id)         -- clave principal
);

/*
fin de creacion de la tabla
*/


/*
comenzamos a crear la tabla clientes
*/

CREATE TABLE clientes(
  id int,
  nombre varchar(100),
  apellidos varchar(200),
  poblacion varchar(100) DEFAULT "santander", -- valor por defecto "Santander"
  telefono varchar(20),
  correo varchar(100) NOT NULL,               -- el correo es obligario
  PRIMARY KEY(id),
  CONSTRAINT uk1 UNIQUE KEY(correo)           -- indexado sin duplicados en correo
);

/**
fin de la tabla clientes
**/

/**
Modificar la tabla coches
**/

ALTER TABLE coches
  ADD CONSTRAINT fkCochesClientes FOREIGN KEY(propietario) REFERENCES clientes(id)
    ON DELETE RESTRICT ON UPDATE RESTRICT;

/**
fin de moficacion de coches
*/

/**
introducir datos en la tabla clientes
**/
INSERT INTO clientes (id, nombre, apellidos, poblacion, telefono, correo)
  VALUES 
    (1, 'eva', 'gomez', 'laredo', '942942942', 'eva@alpe.es'),
    (2,'luis','vazquez',DEFAULT,'942932933','luis@alpe.es');

/**
fin de introducir datos en clientes
**/

/**
introducir datos en coches
**/

INSERT INTO coches (id, marca, modelo, añoCompra, usado, propietario)
  VALUES 
    (1, 'seat', 'leon', 2010, FALSE, 1),
    (2,'renault','clio', 2012, TRUE, 1);


/**
mostrar los clientes 
**/
SELECT 
    * -- campos a mostrar (* es todos) 
  FROM 
    clientes; -- tabla a utilizar

/**
mostrar los coches
**/
SELECT 
    id,
    marca,
    modelo,
    añoCompra,
    usado,
    propietario 
  FROM 
    coches;


/**
listar los coches con los datos del dueño
**/

SELECT 
    *                               -- campos a mostrar
  FROM 
    coches co JOIN clientes cl      -- tablas
      ON co.propietario= cl.id;     -- relacion



